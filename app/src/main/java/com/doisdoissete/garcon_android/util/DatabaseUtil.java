package com.doisdoissete.garcon_android.util;

import android.content.Context;

import com.doisdoissete.garcon_android.R;
import com.doisdoissete.garcon_android.cache.OrderCacheManager;
import com.doisdoissete.garcon_android.model.Order;
import com.doisdoissete.garcon_android.model.OrderItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by ranieripieper on 10/27/16.
 */

public class DatabaseUtil {

    private static int MAX_QTDE = 3;
    private static int MAX_ITEMS = 5;

    public static List<Order> createOrder(Context context, Realm realm, int totalOrders, Date date, String status) {
        List<Order> result = new ArrayList<>();
        for (int i = 0; i < totalOrders; i++) {
            Order order = new Order();
            if (date == null) {
                order.setDate(new Date());
            } else {
                order.setDate(date);
            }
            order.setName(getRandomName(context));
            order.setStatus(status);
            List<OrderItem> items = createItems(context);
            order.setItems(new RealmList<>(items.toArray(new OrderItem[items.size()])));
            OrderCacheManager.getInstance().put(realm, order);
            result.add(order);
        }

        return result;
    }

    public static List<Order> createOrder(Context context, Realm realm, int totalOrders) {
        return createOrder(context, realm, totalOrders, new Date(), Order.STATUS_CREATED);
    }

    public static List<OrderItem> createItems(Context context) {
        List<OrderItem> result = new ArrayList<>();
        for (int i = 0; i < randInt(1, MAX_ITEMS); i++) {
            OrderItem item = new OrderItem();
            item.setQtde(randInt(1, MAX_QTDE));
            item.setDescription(getMenuDescription(context, result));
            result.add(item);
        }
        return result;

    }

    private static String getMenuDescription(Context context, List<OrderItem> items) {
        String[] menu = getMenu(context);
        String result = menu[randInt(0, menu.length - 1)];
        while (existDescription(result, items)) {
            result = menu[randInt(0, menu.length - 1)];
        }
        return result;
    }

    private static boolean existDescription(String newItem, List<OrderItem> items) {
        for (OrderItem item : items) {
            if (item.getDescription().equals(newItem)) {
                return true;
            }
        }
        return false;
    }

    public static String[] getMenu(Context context) {
        return context.getResources().getStringArray(R.array.menu);
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static String getRandomName(Context context) {
        String[] names = context.getResources().getStringArray(R.array.names);
        return names[randInt(0, names.length - 1)];
    }

}
