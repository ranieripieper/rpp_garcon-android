package com.doisdoissete.garcon_android.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by ranipieper on 11/12/15.
 */
public class DateUtil {

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM/yyyy");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR_NUMBERS = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("ddMMyyyy");
        }
    };

    public static final ThreadLocal<DateFormat> HOUR_MINUTE = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("HH'h':mm");
        }
    };
}
