package com.doisdoissete.garcon_android;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.doisdoissete.garcon_android.cache.OrderCacheManager;
import com.doisdoissete.garcon_android.cache.base.RealmManager;
import com.doisdoissete.garcon_android.model.Order;
import com.doisdoissete.garcon_android.util.DatabaseUtil;

import java.util.Calendar;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by ranieripieper on 10/27/16.
 */

public class AppAplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        initCrashlytics();
        initDatabase();
        initCalligraphy();
    }

    private void initDatabase() {
        RealmManager.init(this);
        Realm realm = Realm.getDefaultInstance();
        long total = OrderCacheManager.getInstance().count(realm);
        if (total <= 0) {
            //create old orders
            for (int i = 0; i < 20; i++) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, DatabaseUtil.randInt(0, 23));
                cal.set(Calendar.HOUR_OF_DAY, DatabaseUtil.randInt(0, 59));
                cal.add(Calendar.DAY_OF_MONTH, -DatabaseUtil.randInt(1, 3));
                if (cal.get(Calendar.DAY_OF_MONTH) >= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)) {
                    cal.add(Calendar.DAY_OF_MONTH, -1);
                }
                DatabaseUtil.createOrder(this, realm, 1, cal.getTime(), ((Double.valueOf(Math.random()* 100).longValue()) % 2 == 0) ? Order.STATUS_DELIVERIED : Order.STATUS_REJECTED);
            }
        }
        realm.close();
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }


    private void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(BuildConfig.DEBUG)
                        .build())
                .build();
        // Initialize Fabric with the debug-disabled crashlytics.
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, crashlyticsKit, new Answers(), new Crashlytics());
        } else {
            Fabric.with(this, crashlyticsKit);
        }
    }

}
