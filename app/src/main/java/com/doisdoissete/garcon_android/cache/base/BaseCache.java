package com.doisdoissete.garcon_android.cache.base;


import com.google.gson.Gson;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by broto on 11/3/15.
 */
public abstract class BaseCache<T extends RealmObject> implements Cache<T>{

    public BaseCache() {
    }

    private void updateWithoutNulls(Realm realm, T obj, Gson gson) {
        if (obj != null) {
            realm.beginTransaction();
            String json = gson.toJson(obj);
            realm.createOrUpdateObjectFromJson(getReferenceClass(), json);
            realm.commitTransaction();
        }
    }

    private void updateWithoutNulls(Realm realm, List<T> lst, Gson gson) {
        if (lst != null) {
            realm.beginTransaction();
            String json = gson.toJson(lst);
            realm.createOrUpdateAllFromJson(getReferenceClass(), json);
            realm.commitTransaction();
        }
    }

    public List<T> getAll(Realm realm, String sortField, boolean sortAscending) {
        return getAll(realm, sortField, sortAscending ? Sort.ASCENDING : Sort.DESCENDING);
    }

    public List<T> getAll(Realm realm, String sortField, Sort sort) {
        RealmResults allRealmObjects = realm.where(getReferenceClass()).findAllSorted(sortField, sort);
        return copyFromRealm(realm, allRealmObjects);
    }

    @Override
    public T put(Realm realm, T t) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
        return t;
    }

    @Override
    public List<T> putAll(Realm realm, List<T> t) {
        realm.beginTransaction();

        for (RealmObject item : t) {
            realm.copyToRealmOrUpdate(item);
        }

        realm.commitTransaction();

        return t;
    }

    @Override
    public void delete(Realm realm, final Long id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Class<T> type = getReferenceClass();
                T obj = realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst();
                if (obj != null) {
                    obj.deleteFromRealm();
                }
            }
        });
    }

    @Override
    public void delete(Realm realm, final String id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Class<T> type = getReferenceClass();
                T obj = realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst();
                if (obj != null) {
                    obj.deleteFromRealm();
                }
            }
        });
    }

    public T get(Realm realm, String id) {
        return copyFromRealm(realm, getNotCopied(realm, id));
    }

    public T get(Realm realm, Long id) {
        return copyFromRealm(realm, getNotCopied(realm, id));
    }

    @Override
    public T getNotCopied(Realm realm, Long id) {
        Class<T> type = getReferenceClass();
        return realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst();
    }

    @Override
    public T getNotCopied(Realm realm, String id) {
        Class<T> type = getReferenceClass();
        return realm.where(type).equalTo(getPrimaryKeyName(), id).findFirst();
    }

    @Override
    public void deleteAll(Realm realm) {
        realm.beginTransaction();
        realm.where(getReferenceClass()).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    public long count(Realm realm) {
        return realm.where(getReferenceClass()).count();
    }

    @Override
    public String getPrimaryKeyName() {
        return "id";
    }

    public abstract Class<T> getReferenceClass();

    protected T copyFromRealm(Realm realm, T obj) {
        if (obj == null) {
            return null;
        }
        return realm.copyFromRealm(obj);
    }

    protected List<T> copyFromRealm(Realm realm, List<T> obj) {
        if (obj == null) {
            return null;
        }
        return realm.copyFromRealm(obj);
    }
}
