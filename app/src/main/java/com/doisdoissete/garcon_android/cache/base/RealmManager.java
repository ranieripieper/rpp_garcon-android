package com.doisdoissete.garcon_android.cache.base;

import android.content.Context;

import com.doisdoissete.garcon_android.BuildConfig;
import com.doisdoissete.garcon_android.model.Order;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by jeffcailteux on 7/29/15.
 */
public class RealmManager {

    private static final long REALMVERSION = 1;
    private static RealmConfiguration mConfig;

    private RealmManager() {
    }

    public static void init(Context context) {
        if (BuildConfig.DEBUG) {
            mConfig = new RealmConfiguration.Builder(context)
                    .schemaVersion(REALMVERSION)
                    .deleteRealmIfMigrationNeeded()
                    .build();
        } else {
            mConfig = new RealmConfiguration.Builder(context)
                    .schemaVersion(REALMVERSION)
                    .build();
        }

        //Realm.deleteRealm(mConfig);
        Realm.setDefaultConfiguration(mConfig);
    }

    public static void clearDatabase(Realm realm) {
        realm.beginTransaction();
        realm.delete(Order.class);
        realm.commitTransaction();
    }
}
