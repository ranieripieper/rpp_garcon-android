package com.doisdoissete.garcon_android.cache;

import com.doisdoissete.garcon_android.cache.base.BaseCache;
import com.doisdoissete.garcon_android.model.Order;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class OrderCacheManager extends BaseCache<Order> {

    public static AtomicLong primaryKeyValue;

    private static OrderCacheManager instance = new OrderCacheManager();

    String[] DEFAULT_ORDER_FIELDS = {"date"};
    Sort[] DEFAULT_ORDER_SORT = {Sort.DESCENDING};

    private OrderCacheManager() {
    }

    public static OrderCacheManager getInstance() {
        return instance;
    }


    public List<Order> getPendingOrders(Realm realm) {
        String[] orderFields = {"date"};
        Sort[] orderSort = {Sort.ASCENDING};

        List<Order> lst = realm.where(Order.class)
                .equalTo("status", Order.STATUS_CREATED)
                .findAllSorted(orderFields, orderSort);

        return copyFromRealm(realm, lst);
    }

    public List<Order> getNotPendingOrders(Realm realm) {
        String[] orderFields = {"date"};
        Sort[] orderSort = {Sort.DESCENDING};

        List<Order> lst = realm.where(Order.class)
                .notEqualTo("status", Order.STATUS_CREATED)
                .findAllSorted(orderFields, orderSort);

        return copyFromRealm(realm, lst);
    }

    public List<Order> getAll(Realm realm) {
        List<Order> lst = realm.where(Order.class)
                .findAllSorted(DEFAULT_ORDER_FIELDS, DEFAULT_ORDER_SORT);

        return copyFromRealm(realm, lst);
    }

    public Order updateStatus(Realm realm, Long id, String status) {
        realm.beginTransaction();
        Order obj = getNotCopied(realm, id);
        if (obj != null) {
            obj.setStatus(status);
        }

        realm.commitTransaction();
        return obj;
    }

    public Order put(Realm realm, Order order) {
        setPrimaryKeyValue(realm, order);
        return super.put(realm, order);
    }

    @Override
    public List<Order> putAll(Realm realm, List<Order> lst) {
        setPrimaryKeyValue(realm, lst);
        return super.putAll(realm, lst);
    }


    private void setPrimaryKeyValue(Realm realm, List<Order> lst) {
        if (lst != null) {
            for (Order msg : lst) {
                setPrimaryKeyValue(realm, msg);
            }
        }
    }

    private void setPrimaryKeyValue(Realm realm, Order msg) {
        if (msg.getId() == null) {
            if (primaryKeyValue == null) {
                Number maxId = realm.where(Order.class).max("id");
                if (maxId == null) {
                    maxId = 0;
                }
                primaryKeyValue = new AtomicLong(maxId.longValue());
            }
            msg.setId(primaryKeyValue.incrementAndGet());
        }
    }

    @Override
    public Class<Order> getReferenceClass() {
        return Order.class;
    }
}
