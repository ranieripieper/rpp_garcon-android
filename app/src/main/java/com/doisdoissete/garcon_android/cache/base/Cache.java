package com.doisdoissete.garcon_android.cache.base;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by broto on 8/16/15.
 */
public interface Cache<T extends RealmObject> {
    T put(Realm realm, T t);

    List<T> putAll(Realm realm, List<T> t);

    void delete(Realm realm, String id);

    void delete(Realm realm, Long id);

    void deleteAll(Realm realm);

    T getNotCopied(Realm realm, String id);

    T getNotCopied(Realm realm, Long id);

    T get(Realm realm, Long id);

    T get(Realm realm, String id);

    long count(Realm realm);

    String getPrimaryKeyName();
}
