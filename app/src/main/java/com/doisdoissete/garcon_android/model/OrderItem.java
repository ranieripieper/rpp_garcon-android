package com.doisdoissete.garcon_android.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

/**
 * Created by ranieripieper on 10/27/16.
 */

public class OrderItem extends RealmObject {

    @Expose
    private int qtde;
    @Expose
    private String description;

    /**
     * Gets the qtde
     *
     * @return qtde
     */
    public int getQtde() {
        return qtde;
    }

    /**
     * Sets the qtde
     *
     * @param qtde
     */
    public void setQtde(int qtde) {
        this.qtde = qtde;
    }

    /**
     * Gets the description
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
