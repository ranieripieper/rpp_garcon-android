package com.doisdoissete.garcon_android.model;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TypefaceSpan;

import com.doisdoissete.garcon_android.util.CustomTypefaceSpan;
import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ranieripieper on 10/27/16.
 */

public class Order extends RealmObject {

    public static String STATUS_CREATED = "created";
    public static String STATUS_REJECTED = "rejected";
    public static String STATUS_DELIVERIED = "deliveried";

    @Expose
    @PrimaryKey
    private Long id;

    @Expose
    private String name;

    @Expose
    private Date date;

    @Expose
    private String status;

    @Expose
    private RealmList<OrderItem> items;

    /**
     * Gets the id
     *
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the id
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the name
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the date
     *
     * @return date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the status
     *
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets the items
     *
     * @return items
     */
    public RealmList<OrderItem> getItems() {
        return items;
    }

    /**
     * Sets the items
     *
     * @param items
     */
    public void setItems(RealmList<OrderItem> items) {
        this.items = items;
    }

    public CharSequence getOrderItemsToView(Context context) {
        CharSequence result = "";

        for (OrderItem item : getItems()) {
            String txtItem = String.format("%s %s\n", item.getQtde(), item.getDescription());

            Typeface bold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Bold.ttf");
            TypefaceSpan boldSpan = new CustomTypefaceSpan("", bold);
            SpannableString spannableString = new SpannableString(txtItem);
            spannableString.setSpan(boldSpan, 0, txtItem.indexOf(" "), 0);

            result = TextUtils.concat(result, spannableString);
        }

        return result;
    }
}
