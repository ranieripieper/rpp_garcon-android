package com.doisdoissete.garcon_android.view;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.doisdoissete.garcon_android.R;

import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by ranieripieper on 10/27/16.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private Realm mRealm;

    protected Toolbar mToolbar;

    @Override
    protected void onPause() {
        closeRealm();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        closeRealm();
        super.onDestroy();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        initToolbar();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        initToolbar();
    }

    private void initToolbar() {
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    protected void closeRealm() {
        if (mRealm != null) {
            mRealm.close();
        }
    }
    protected Realm getRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        return mRealm;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
