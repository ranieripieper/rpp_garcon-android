package com.doisdoissete.garcon_android.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.garcon_android.R;
import com.doisdoissete.garcon_android.cache.OrderCacheManager;
import com.doisdoissete.garcon_android.cache.base.RealmManager;
import com.doisdoissete.garcon_android.model.Order;
import com.doisdoissete.garcon_android.util.DatabaseUtil;
import com.doisdoissete.garcon_android.view.adapter.OrderAdapter;

import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class MainActivity extends BaseActivity {

    private RecyclerView mRecyclerView;
    private OrderAdapter mAdapter;

    private View mLayoutNewOrder;
    private TextView mTxtTitle;

    private Handler mHandler = new Handler();

    private int TIME_ADD_ORDER = 45000;
    private Boolean layout_new_order_visible = true;
    private int LAYOUT_NEW_ORDER_SHOW_HIDE = 200;

    private int MAX_NR_ORDER = 20;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            mRecyclerView.setItemAnimator(new SlideInUpAnimator());
            if (mAdapter.getItemCount() < MAX_NR_ORDER) {
                List<Order> orders = DatabaseUtil.createOrder(MainActivity.this, getRealm(), 1);

                mAdapter.addAll(orders);

                if (hasScrollBottom()) {
                    showLayoutNewOrder();
                }
            }

            addOrder(TIME_ADD_ORDER);
        }
    };

    private boolean hasScrollBottom() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("");
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutNewOrder = findViewById(R.id.layout_new_order);
        mTxtTitle = (TextView)findViewById(R.id.txt_title);

        mTxtTitle.setText(getString(R.string.hello, DatabaseUtil.getRandomName(this)));
        populateOrders();

        closeLayoutNewOrder(1);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                closeLayoutNewOrder();
            }
        });
    }

    private void showLayoutNewOrder() {
        synchronized (layout_new_order_visible) {
            if (layout_new_order_visible) {
                return;
            }
            mLayoutNewOrder.bringToFront();
            mLayoutNewOrder.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            ViewCompat.animate(mLayoutNewOrder).translationY(0).setDuration(LAYOUT_NEW_ORDER_SHOW_HIDE);
            layout_new_order_visible = true;
        }
    }

    private void closeLayoutNewOrder(int duration) {
        synchronized (layout_new_order_visible) {
            if (!layout_new_order_visible) {
                return;
            }

            mLayoutNewOrder.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            ViewCompat.animate(mLayoutNewOrder).translationY(mLayoutNewOrder.getMeasuredHeight()).setDuration(duration);
            layout_new_order_visible = false;
        }
    }

    private void closeLayoutNewOrder() {
        closeLayoutNewOrder(LAYOUT_NEW_ORDER_SHOW_HIDE);
    }
    private void populateOrders() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setItemAnimator(new SlideInUpAnimator());

        mAdapter = new OrderAdapter(this, getInitalList(), new OrderAdapter.OrderAdapterListener() {
            @Override
            public void deliveryOrder(Order order) {
                updateStatusAndRemove(order, Order.STATUS_DELIVERIED);
            }

            @Override
            public void rejectOrder(Order order) {
                updateStatusAndRemove(order, Order.STATUS_REJECTED);
            }

            @Override
            public void eastegg() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Apagar histórico?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                RealmManager.clearDatabase(getRealm());
                                mAdapter.removeAll();
                                mAdapter.notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                builder.create().show();
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }

    private void cancelAddOrders() {
        mHandler.removeCallbacks(mRunnable);
    }

    private void updateStatusAndRemove(Order order, String status) {
        cancelAddOrders();
        order.setStatus(status);
        OrderCacheManager.getInstance().put(getRealm(), order);
        if (Order.STATUS_DELIVERIED.equals(status)) {
            mRecyclerView.setItemAnimator(new SlideInRightAnimator());
        } else {
            mRecyclerView.setItemAnimator(new SlideInLeftAnimator());
        }
        mAdapter.removeItem(order.getId());

        addOrder(TIME_ADD_ORDER / 3);
    }

    @Override
    protected void onPause() {
        cancelAddOrders();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        addOrder(1000);
    }

    private void addOrder(final int time) {
        cancelAddOrders();
        mHandler.postDelayed(mRunnable, time);
    }

    private List<Order> getInitalList() {
        List<Order> orders = OrderCacheManager.getInstance().getPendingOrders(getRealm());
        return orders;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_history:
                startActivity(new Intent(this, HistoryActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        return true;
    }
}
