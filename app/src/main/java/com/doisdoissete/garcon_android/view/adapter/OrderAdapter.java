package com.doisdoissete.garcon_android.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.doisdoissete.garcon_android.R;
import com.doisdoissete.garcon_android.model.Order;
import com.doisdoissete.garcon_android.util.DateUtil;

import java.util.List;

import io.realm.Realm;

/**
 * Created by ranieripieper on 10/27/16.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private List<Order> mDataset;
    private OrderAdapterListener mListener;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected View mView;
        public TextView mTxtDate;
        public TextView mTxtName;
        public TextView mTxtOrder;
        public Button mBtReject;
        public Button mBtDelivery;

        public ViewHolder(View v) {
            super(v);
            mView = v;
            mTxtDate = (TextView) v.findViewById(R.id.txt_date);
            mTxtName = (TextView) v.findViewById(R.id.txt_name);
            mTxtOrder = (TextView) v.findViewById(R.id.txt_order);
            mBtReject = (Button) v.findViewById(R.id.btn_reject);
            mBtDelivery = (Button) v.findViewById(R.id.btn_delivery);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderAdapter(Context context, List<Order> mData, OrderAdapterListener homeAdapterListener) {
        mContext = context;
        mDataset = mData;
        mListener = homeAdapterListener;
        Realm realm = Realm.getDefaultInstance();
        realm.close();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_order_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Order obj = mDataset.get(position);

        holder.mTxtDate.setText(DateUtil.HOUR_MINUTE.get().format(obj.getDate()));
        holder.mTxtName.setText(obj.getName());
        holder.mTxtOrder.setText(obj.getOrderItemsToView(mContext));

        holder.mBtDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.deliveryOrder(obj);
            }
        });

        holder.mBtReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.rejectOrder(obj);
            }
        });

        holder.mTxtName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mListener.eastegg();
                return false;
            }
        });

    }

    public Order getItem(int position) {
        return mDataset.get(position);
    }

    public void removeItem(long id) {
        int position = -1;
        for (int i = 0; i < getItemCount(); i++) {
            if (getItem(i).getId().equals(id)) {
                position = i;
                break;
            }
        }

        if (position >= 0 && position < getItemCount()) {
            mDataset.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeAll() {
        mDataset.removeAll(mDataset);
    }

    public void addAll(List<Order> lst) {
        mDataset.addAll(mDataset.size(), lst);
        notifyItemRangeInserted(mDataset.size()-lst.size(), mDataset.size());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset != null ? mDataset.size() : 0;
    }

    public interface OrderAdapterListener {
        void deliveryOrder(Order order);
        void rejectOrder(Order order);
        void eastegg();
    }
}