package com.doisdoissete.garcon_android.view.adapter;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.doisdoissete.garcon_android.R;
import com.doisdoissete.garcon_android.model.Order;
import com.doisdoissete.garcon_android.util.DateUtil;

import java.util.Calendar;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by ranieripieper on 10/27/16.
 */

public class HistoryOrderAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private Context mContext;
    private List<Order> mOrders;
    private LayoutInflater inflater;

    public HistoryOrderAdapter(Context context, List<Order> orders) {
        inflater = LayoutInflater.from(context);
        mOrders = orders;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mOrders.size();
    }

    @Override
    public Order getItem(int position) {
        return mOrders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_order_item_history, parent, false);
            holder.mTxtDate = (TextView) convertView.findViewById(R.id.txt_date);
            holder.mTxtName = (TextView) convertView.findViewById(R.id.txt_name);
            holder.mTxtOrder = (TextView) convertView.findViewById(R.id.txt_order);
            holder.mBtAction = (Button) convertView.findViewById(R.id.btn_action);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Order obj = getItem(position);

        holder.mTxtDate.setText(DateUtil.HOUR_MINUTE.get().format(obj.getDate()));
        holder.mTxtName.setText(obj.getName());
        holder.mTxtOrder.setText(obj.getOrderItemsToView(mContext));

        if (obj.getStatus().equals(Order.STATUS_DELIVERIED)) {
            holder.mBtAction.setText(R.string.delivery_order);
            holder.mBtAction.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.positiveButtonDisabled, null));
        } else {
            holder.mBtAction.setText(R.string.rejected_order);
            holder.mBtAction.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.negativeButtonDisabled, null));
        }
        holder.mBtAction.setEnabled(false);

        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.header_row_order_item_history, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.txt_header);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        Order order = getItem(position);
        holder.text.setText(getHeader(order));

        return convertView;
    }

    private String getHeader(Order order) {
        Calendar dt = Calendar.getInstance();
        Calendar dtOrder = Calendar.getInstance();
        dtOrder.setTime(order.getDate());

        if (DateUtil.DATE_DAY_MONTH_YEAR_NUMBERS.get().format(dt.getTime())
                .equalsIgnoreCase(DateUtil.DATE_DAY_MONTH_YEAR_NUMBERS.get().format(order.getDate()))) {
            return mContext.getString(R.string.today);
        } else if (daysBetween(dt, dtOrder) <= 1) {
            return mContext.getString(R.string.yesterday);
        } else {
            return DateUtil.DATE_DAY_MONTH_YEAR.get().format(order.getDate());
        }
    }

    public static int daysBetween(Calendar day1, Calendar day2){
        Calendar dayOne = (Calendar) day1.clone(),
                dayTwo = (Calendar) day2.clone();

        if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
            return Math.abs(dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR));
        } else {
            if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
                //swap them
                Calendar temp = dayOne;
                dayOne = dayTwo;
                dayTwo = temp;
            }
            int extraDays = 0;

            int dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR);

            while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                dayOne.add(Calendar.YEAR, -1);
                // getActualMaximum() important for leap years
                extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
            }

            return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays ;
        }
    }

    @Override
    public long getHeaderId(int position) {
        return Long.valueOf(DateUtil.DATE_DAY_MONTH_YEAR_NUMBERS.get().format(getItem(position).getDate()));
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        TextView mTxtDate;
        TextView mTxtName;
        TextView mTxtOrder;
        Button mBtAction;
    }

}