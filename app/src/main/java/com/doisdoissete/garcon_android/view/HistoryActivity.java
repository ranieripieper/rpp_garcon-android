package com.doisdoissete.garcon_android.view;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;

import com.doisdoissete.garcon_android.R;
import com.doisdoissete.garcon_android.cache.OrderCacheManager;
import com.doisdoissete.garcon_android.view.adapter.HistoryOrderAdapter;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class HistoryActivity extends BaseActivity {

    private StickyListHeadersListView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        mRecyclerView = (StickyListHeadersListView) findViewById(R.id.list);
        HistoryOrderAdapter adapter = new HistoryOrderAdapter(this, OrderCacheManager.getInstance().getNotPendingOrders(getRealm()));
        mRecyclerView.setAdapter(adapter);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.icn_back, null));
        setTitle("");
    }
}
